/*
 * This is a simple kioslave to handle mbox-files.
 * Copyright (C) 2004 Mart Kelder (mart.kde@hccnet.nl)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "stat.h"

#include "readmbox.h"
#include "urlinfo.h"


KIO::UDSEntry Stat::stat(const UrlInfo *info, ReadMBox *mbox)
{
    KIO::UDSEntry entry;
    const UrlInfo::UrlType type =(info!=nullptr ? info->type() : UrlInfo::directory);

    if (type==UrlInfo::directory && mbox==nullptr)
    {
        // Specific entries for a directory
        entry.fastInsert(KIO::UDSEntry::UDS_FILE_TYPE, S_IFDIR);
        entry.fastInsert(KIO::UDSEntry::UDS_NAME, (info!=nullptr ? info->filename() : "."));
        entry.fastInsert(KIO::UDSEntry::UDS_ACCESS, S_IRUSR|S_IXUSR|S_IRGRP|S_IXGRP|S_IROTH|S_IXOTH);
    }
    else
    {
        Q_ASSERT(info!=nullptr);

        // Specific entries for a message
        entry.fastInsert(KIO::UDSEntry::UDS_FILE_TYPE, S_IFREG);
        entry.fastInsert(KIO::UDSEntry::UDS_MIME_TYPE, "message/rfc822");
        entry.fastInsert(KIO::UDSEntry::UDS_ACCESS, S_IRUSR|S_IRGRP|S_IROTH);

        if (mbox!=nullptr)				// reading from mailbox file
        {
            if (info->type()==UrlInfo::message) mbox->searchMessage(info->id());

            const QString id = mbox->currentID();
            const QString url = QString("mbox:%1/%2").arg(info->filename(), id);
            entry.fastInsert(KIO::UDSEntry::UDS_URL, url);
            entry.fastInsert(KIO::UDSEntry::UDS_NAME, id);

            if (!id.isEmpty())
            {
                // The ID is of the form "From SolDCNewsletter@Sun.COM Fri Jan 26 04:52:56 2001"
                const QStringList fields = mbox->currentID().split(' ');
                const QString sender = fields.value(1);
                if (!sender.isEmpty()) entry.fastInsert(KIO::UDSEntry::UDS_DISPLAY_NAME, sender);
            }

            entry.fastInsert(KIO::UDSEntry::UDS_SIZE, mbox->skipMessage());
        }
        else						// no file, just basic info
        {
            QString url = QString("mbox:%1").arg(info->url());
            entry.fastInsert(KIO::UDSEntry::UDS_URL, url);
            url = url.right(url.length()-url.lastIndexOf('/')-1);
            entry.fastInsert(KIO::UDSEntry::UDS_NAME, url);
        }
    }

    return (entry);
}
