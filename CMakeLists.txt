project(kio-mbox)

cmake_minimum_required (VERSION 2.8.12 FATAL_ERROR)
set(QT_MIN_VERSION "5.12.0")
set(KF5_MIN_VERSION "5.68.0")
set(ECM_MIN_VERSION "5.68.0")

# ECM setup (Extra Cmake Modules)
find_package(ECM ${ECM_MIN_VERSION} REQUIRED NO_MODULE)
set(CMAKE_MODULE_PATH ${ECM_MODULE_PATH} ${ECM_KDE_MODULE_DIR})

include(FeatureSummary)
include(ECMSetupVersion)
include(KDEInstallDirs)
include(KDECompilerSettings NO_POLICY_SCOPE)
include(KDECMakeSettings)
include(ECMQtDeclareLoggingCategory)

# Required Qt5 components to build this package
find_package(Qt5 ${QT_MIN_VERSION} REQUIRED COMPONENTS Core)

# Required KDE Frameworks components to build this package
find_package(KF5 ${KF5_MIN_VERSION} REQUIRED COMPONENTS KIO I18n)

# Rigourousness
add_definitions("-DQT_USE_FAST_CONCATENATION")
add_definitions("-DQT_USE_FAST_OPERATOR_PLUS")
add_definitions("-DQT_NO_CAST_FROM_BYTEARRAY")
add_definitions("-DQT_NO_NARROWING_CONVERSIONS_IN_CONNECT")
add_definitions("-DQT_NO_CAST_TO_ASCII")
add_definitions("-DQT_NO_URL_CAST_FROM_STRING")

# Permissiveness
remove_definitions("-DQT_NO_CAST_FROM_ASCII")
remove_definitions("-DQT_NO_SIGNALS_SLOTS_KEYWORDS")

########### KIO-MBOX ###############

add_definitions("-DTRANSLATION_DOMAIN=\"\\\"kio5_mbox\\\"\"")

set(kio_mbox_SRCS
   mbox.cpp
   mboxfile.cpp
   readmbox.cpp
   stat.cpp
   urlinfo.cpp
)

ecm_qt_declare_logging_category(kio_mbox_SRCS
  HEADER "mbox_logging.h"
  IDENTIFIER "MBOX_LOG"
  CATEGORY_NAME "kio-mbox"
  EXPORT mboxlogging
  DESCRIPTION "kioslave (MBOX)")

add_library(kio_mbox MODULE ${kio_mbox_SRCS} )
target_link_libraries(kio_mbox Qt5::Core KF5::KIOCore KF5::I18n)
set_target_properties(kio_mbox PROPERTIES OUTPUT_NAME "mbox")

########### install files ###############

install(TARGETS kio_mbox DESTINATION ${KDE_INSTALL_PLUGINDIR}/kf5/kio)
install(FILES mbox.protocol DESTINATION ${KDE_INSTALL_KSERVICES5DIR})

ecm_qt_install_logging_categories(
  EXPORT mboxlogging
  FILE kio-mbox.categories
  DESTINATION "${KDE_INSTALL_LOGGINGCATEGORIESDIR}")

############### configuration information ###############

feature_summary(WHAT ALL FATAL_ON_MISSING_REQUIRED_PACKAGES)
