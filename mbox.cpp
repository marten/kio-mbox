/*
 * This is a simple kioslave to handle mbox-files.
 * Copyright (C) 2004 Mart Kelder (mart.kde@hccnet.nl)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "mbox.h"

#include <qcoreapplication.h>

#include <klocalizedstring.h>

#include "readmbox.h"
#include "stat.h"
#include "urlinfo.h"
#include "mbox_logging.h"


extern "C" int Q_DECL_EXPORT kdemain(int argc, char **argv)
{
    QCoreApplication app(argc, argv);
    app.setApplicationName(QLatin1String("kio_mbox"));

    if (argc!=4)
    {
        fprintf(stderr, "Usage: kio_mbox protocol domain-socket1 domain-socket2\n");
        exit(-1);
    }

    MBoxProtocol slave(argv[1], argv[2], argv[3]);
    slave.dispatchLoop();
    return (0);
}


MBoxProtocol::MBoxProtocol(const QByteArray &proto, const QByteArray &poolSocket,
                           const QByteArray &appSocket)
    : TCPSlaveBase(proto.toLower(), poolSocket, appSocket)
{
    qCDebug(MBOX_LOG);
    m_errorState = true;
}


void MBoxProtocol::get( const QUrl& url )
{
    qCDebug(MBOX_LOG) << url;

    m_errorState = false;

    UrlInfo info( url, UrlInfo::message );
    QString line;
    QByteArray ba_line;

    if( info.type() == UrlInfo::invalid && !m_errorState ) {
        error( KIO::ERR_DOES_NOT_EXIST, info.url() );
        return;
    }

    mimeType("message/rfc822");

    ReadMBox mbox( &info, this );
    while( !mbox.atEnd() && !m_errorState) {
        line = mbox.currentLine();
        line += '\n';
        data(line.toUtf8());
        mbox.nextLine();
    };

    if( !m_errorState ) {
        data( QByteArray() );
        finished();
    }
}


void MBoxProtocol::listDir( const QUrl& url )
{
    qCDebug(MBOX_LOG) << url;

    m_errorState = false;

    KIO::UDSEntry entry;
    UrlInfo info( url, UrlInfo::directory );
    ReadMBox mbox( &info, this, hasMetaData( "onlynew" ), hasMetaData( "savetime" ) );

    if( m_errorState ) {
        return;
    }

    if( info.type() != UrlInfo::directory ) {
        error( KIO::ERR_DOES_NOT_EXIST, info.url() );
        return;
    }

    listEntry(Stat::stat(nullptr));			// entry for "."
    while(!mbox.atEnd() && !m_errorState)
    {
        entry = Stat::stat(&info, &mbox);		// entries for messages
        if (mbox.inListing()) listEntry(entry);
    }

    finished();
}


void MBoxProtocol::stat( const QUrl& url )
{
    qCDebug(MBOX_LOG) << url;

    UrlInfo info( url );
    if( info.type() == UrlInfo::invalid ) {
        error( KIO::ERR_DOES_NOT_EXIST, url.path() );
        return;
    }

    statEntry(Stat::stat(&info));
    finished();
}


void MBoxProtocol::mimetype( const QUrl& url )
{
    qCDebug(MBOX_LOG) << url;

    m_errorState = false;

    UrlInfo info( url );

    if( m_errorState ) {
        return;
    }

    if( info.type() == UrlInfo::invalid ) {
        error( KIO::ERR_DOES_NOT_EXIST, i18n( "Invalid URL" ) );
    } else {
        mimeType( info.mimetype() );
    }
    finished();
}


void MBoxProtocol::emitError( int _errno, const QString& arg )
{
    qCDebug(MBOX_LOG) << _errno << arg;

    m_errorState = true;
    error( _errno, arg );
}
