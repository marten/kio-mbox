/*
 * This is a simple kioslave to handle mbox-files.
 * Copyright (C) 2004 Mart Kelder (mart.kde@hccnet.nl)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef STAT_H
#define STAT_H

#include <kio/udsentry.h>

class ReadMBox;
class UrlInfo;


/**
 * This namespace provides a function to get information for a
 * mailbox file or message.
 **/
namespace Stat
{
    /**
     * Read the status of a mailbox or message, and return a @c UDSEntry
     * with all of the available information.
     *
     * @param info The @c UrlInfo with the URL and information for the
     * entry, or @c nullptr to return information for the "." directory
     * only.
     * @param mbox The mailbox reader to use to provide information on a
     * message, or @c nullptr for basic information only.
     * @note If the @p mbox parameter is not a @c nullptr, then @p info
     * must also not be a @c nullptr.
     * @return the @c UDSEntry with all available information filled in.
     **/
    KIO::UDSEntry stat(const UrlInfo *info, ReadMBox *mbox = nullptr);
};

#endif
